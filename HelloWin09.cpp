/* Code practice 09 */

#include <Windows.h>

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")

/* Prototype for callback procedure */

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/*Entry point Procedure */

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	/* Define Window Class */
	static TCHAR szClassName[] = TEXT("The First Window");

	/* Define Window Caption */
	static TCHAR szWindowCaption[] = TEXT("Hello Win 09");

	HBRUSH hBrush = NULL; 	/* Handle to a brush */
	HCURSOR hCursor = NULL; /* Handle to a Cursor */
	HICON hIcon = NULL; 	/* Handle to an Icon */
	HICON hIconSm = NULL;	/* Handle to an Icon */
	HWND hWnd = NULL;

	WNDCLASSEX wndEx;
	MSG msg;

	ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory(&msg, sizeof(MSG));

	hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	if(hBrush == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get a handle to a brush"), TEXT("GetStockObject"), MB_ICONERROR);
		return(EXIT_FAILURE); 
	}

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
	if(hCursor == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get a handle to cusor"), TEXT("LoadCursor"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);	
	if(hIcon == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get a handle to an Icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}
		
	hIconSm= LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if(hIconSm == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get a handle to an Icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return(EXIT_FAILURE);	
	}

	wndEx.cbSize = sizeof(WNDCLASSEX);
	wndEx.cbClsExtra = 0;
	wndEx.cbWndExtra = 0;
	wndEx.hbrBackground = hBrush;
	wndEx.hCursor = hCursor;
	wndEx.hIcon = hIcon;
	wndEx.hIconSm = hIconSm;
	
	wndEx.hInstance =  hInstance;

	wndEx.lpszClassName = szClassName;
	wndEx.lpszMenuName = NULL;
	wndEx.lpfnWndProc = WndProc;	

	wndEx.style= CS_HREDRAW | CS_VREDRAW;

	/* Register Window Class */
	if(!RegisterClassEx(&wndEx))
	{
		MessageBox((HWND)NULL, TEXT("Failed to register window class"), TEXT("RegisterClassEx"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	hWnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		szWindowCaption,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		(HWND)NULL,
		(HMENU)NULL,
		hInstance,
		NULL

		);
	if(hWnd == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to create a window"), TEXT("CreateWindowEx"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	ShowWindow(hWnd, nShowCmd);
	UpdateWindow(hWnd);

	while(GetMessage(&msg, NULL, 0 , 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return(msg.wParam);
}

/*****************************************************************************/
//Callback Procedure Definition
/*****************************************************************************/

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_DESTROY:
			PostQuitMessage(EXIT_SUCCESS);
			break;
	}

	return(DefWindowProc(hWnd, uMsg, wParam, lParam));
}
