/* Master Header file that inlcudes other header files containing
1. Symbolic constants
2. Typedefs
3. Functions Prototypes
used in this program.
*/

#include <windows.h>

/* Link substystem DLL files contains stubs of Win32 API's */
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")

/* Prototype of callback procedure of Window */
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/* Entry Point Procedure */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    /* Define Window Class Name */
    static TCHAR szClassName[]= TEXT("The First Window 01");

    /* Define Window Caption */
    static TCHAR szWindowCaption[] = TEXT("Masterclass in C - Batch I - GUI 01");

    HBRUSH hBrush   = NULL; /* Handle to Brush */
    HCURSOR hCursor = NULL; /* Handle to Cursor */
    HICON hIcon     = NULL; /* Handle to Icon */
    HICON hIconSm   = NULL; /* Handle to Small Icon */
    HWND hWnd       = NULL;

    WNDCLASSEX wndEx;
    MSG msg;

    ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
    ZeroMemory(&msg, sizeof(MSG));

    hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
    if(hBrush == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to acquire brush handle"), TEXT("GetStockObject"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    hCursor= LoadCursor((HINSTANCE)NULL, IDC_ARROW);
    if(hCursor == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to acquire a Cursor"), TEXT("LoadCursor"), MB_ICONERROR);
        return (EXIT_FAILURE);
    }

    hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
    if(hIcon == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to acquire an Icon"), TEXT("LoadIcon"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
    if(hIconSm == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to acquire icon"), TEXT("LoadIcon"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    wndEx.cbSize        = sizeof(WNDCLASSEX);
    wndEx.cbClsExtra    = 0;
    wndEx.cbWndExtra    = 0;

    wndEx.hbrBackground = hBrush;
    wndEx.hCursor       = hCursor;
    wndEx.hIcon         = hIcon;
    wndEx.hIconSm       = hIconSm;

    wndEx.hInstance     = hInstance;

    wndEx.lpszClassName = szClassName;
    wndEx.lpszMenuName  = NULL;
    wndEx.lpfnWndProc   = WndProc;

    wndEx.style = CS_HREDRAW | CS_VREDRAW;

    if(!RegisterClassEx(&wndEx))
    {
        MessageBox((HWND)NULL, TEXT("Failed to register window class"), TEXT("RegisterClassEx"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    hWnd = CreateWindowEx(

        WS_EX_APPWINDOW,    /* Unsigned int */
        szClassName,        /* LPCWSTR */
        szWindowCaption,    /* LPCWSTR */
        WS_OVERLAPPEDWINDOW, /* WS_ORLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | THICHFRAME*/
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        (HWND)NULL,
        (HMENU)NULL,
        hInstance,
        NULL
    );

    if(hWnd == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to Create Window"), TEXT("CreateWindow"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    ShowWindow(hWnd, nShowCmd);
    UpdateWindow(hWnd);

    while(GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return(msg.wParam);

}

//*****************************************************************************
//callback
//*****************************************************************************

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        case WM_DESTROY:
            PostQuitMessage(EXIT_SUCCESS);
            break;
    }

    return(DefWindowProc(hWnd, uMsg, wParam, lParam));
}