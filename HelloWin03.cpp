/* Master Header file that includes other header files containing
1. Symbolic constans
2. Typedefs
3. Functions Prototypes
used in this program.
*/

#include <Windows.h>

/* Link subsystem DLL Files that contains stubs of Win32 API's */
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")

/* Prototype of callback procedureof windows */
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

//*****************************************************************************
//Etnry Point Procedure
//*****************************************************************************

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	/* Define window Class Name */
	static TCHAR szClassName[]= TEXT("The first Window");

	/* Define Window Caption */

	static TCHAR szWindowCaption[]= TEXT("Masterclass in C - Batch 1 - GUI 1");

	HBRUSH hBrush 	= NULL; /* Handle to a bruash */
	HCURSOR hCursor = NULL; /* Handle to a cursor */
	HICON hIcon  	= NULL; /* Handle to an Icon */
	HICON hIconSm  	= NULL; /* Handle to a small Icon */
	HWND hWnd  		= NULL;

	WNDCLASSEX wndEx;
	MSG msg;

	ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory(&msg, sizeof(MSG));

	hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
	if(hBrush == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get hBrush Handle"), TEXT("GetStockObject"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
	if(hCursor == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get cursor handle"), TEXT("LoadCursor"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if(hIcon == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get icon Handle"), TEXT("LoadIcon"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if(hIconSm == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to get icon handle"), TEXT("LoadIcon"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	wndEx.cbSize 		= sizeof(WNDCLASSEX);
	wndEx.cbClsExtra 	= 0;
	wndEx.cbWndExtra 	= 0;

	wndEx.hbrBackground = hBrush;
	wndEx.hCursor 		= hCursor;
	wndEx.hIcon 		= hIcon;
	wndEx.hIconSm 		= hIconSm;

	wndEx.hInstance  	= hInstance;

	wndEx.lpszClassName = szClassName;
	wndEx.lpszMenuName 	= NULL;
	wndEx.lpfnWndProc  	= WndProc;

	wndEx.style 		= CS_HREDRAW | CS_VREDRAW;

	if(!RegisterClassEx(&wndEx))
	{
		MessageBox((HWND)NULL, TEXT("Failed to register window Class"), TEXT("RegisterClassEx"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}


	hWnd = CreateWindowEx(

		WS_EX_APPWINDOW,	/* Unsigned int */
		szClassName,		/* LPCWSTR */
		szWindowCaption,	/* LPCWSTR */
		WS_OVERLAPPEDWINDOW, /* WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME */
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		(HWND)NULL,
		(HMENU)NULL,
		hInstance,
		NULL
		);

	if(hWnd == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Failed to create Window"), TEXT("CreateWindow"), MB_ICONERROR);
		return(EXIT_FAILURE);
	}

	ShowWindow(hWnd,nShowCmd);
	UpdateWindow(hWnd);

	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	
	return (msg.wParam);
}
//************************************************************************************
//callback procedure
//************************************************************************************

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_DESTROY:
			PostQuitMessage(EXIT_SUCCESS);
			break;
	}

	return(DefWindowProc(hWnd, uMsg, wParam, lParam));
}



